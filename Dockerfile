FROM archlinux:latest
# resetting all gnupg keys
RUN rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate archlinux
# prepare pacman for use and generic dependencies
RUN pacman -Syyuu --noconfirm \
    archlinux-keyring \
    ca-certificates
# install specific dependencies we need
RUN pacman -S --noconfirm \
    biber \
    texlive-most \
    minted \
    which
# symlink biber from its strange path in /usr/bin/vendor_perl/biber to /usr/bin
RUN ln -s /usr/bin/vendor_perl/biber /usr/bin/biber
# copy all our files into the container ignoring .dockerignore'd files
COPY . /latex-src
# setting workdir to the src dir
WORKDIR /latex-src
# creating standard user to interact with files
RUN useradd archie && \
    chown -R  archie:archie /latex-src && \
    chmod 744 build.sh
USER archie
# setting default command to run if none given
CMD ["./build.sh"]
